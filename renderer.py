import pygame


def renderWorld(window, world):
    clearWindow(window)
    particles = world.getParticles()

    for particle in particles:
        pos = particle.getPos()

        n = len(particle.getNeighbours())

        # color = pygame.Color(int((255 - 255/(n+1))/2),255- int(255 - 255/(n+1)),int((255 - 255/(n+1))/2), 255)

        color = pygame.Color(0, 255, 0)

        if 5 < n < 15:
            color = pygame.Color(150, 200, 0)
        elif 15 < n < 30:
            color = pygame.Color(150, 150, 0)
        elif 30 < n < 50:
            color = pygame.Color(200, 150, 0)
        elif 50 < n < 70:
            color = pygame.Color(200, 50, 0)
        elif n > 70:
            color = pygame.Color(255, 0, 50)

        pygame.draw.circle(window, color, (pos.x, pos.y), world.particleSize)


def clearWindow(window):
    window.fill((0, 0, 0))
