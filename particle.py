from vector import Vec2
from functions import *


class Particle:

    def __init__(self, x, y, o):
        self.pos = Vec2(x, y)
        self.orientation = o
        self.neighbours = []

    def setPos(self, x, y):
        self.pos = Vec2(x, y)

    def getPos(self):
        return self.pos

    def addNeighbours(self, n):
        self.neighbours.append(n)

    def clearNeighbours(self):
        self.neighbours = []

    def getNeighbours(self):
        return self.neighbours

    def move(self, rotation, speed, attractionForce):

        direction = self.rotateTowards(rotation, attractionForce)
        self.orientation += direction

        if self.orientation > 360:
            self.orientation -= 360
        elif self.orientation < 0:
            self.orientation += 360

        direction = degreeToVector2(self.orientation)

        x = direction.x * speed
        x += self.pos.x
        y = direction.y * speed
        y += self.pos.y

        self.setPos(x, y)

    def rotateTowards(self, rotation, beta):
        left = 0
        right = 0

        neighboursCount = len(self.neighbours)

        direction = degreeToVector2(self.orientation + rotation)

        for particle in self.neighbours:
            oPos = particle.getPos()
            angle = angleFrom3Vectors(self.pos, direction, oPos)

            if angle < 0:
                left += 1
            elif angle > 0:
                right += 1

        signe = 0
        if right - left > 0:
            signe = 1
        elif right - left < 0:
            signe = -1

        return rotation + beta * neighboursCount * signe
