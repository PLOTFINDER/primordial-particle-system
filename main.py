import pygame
import time
from renderer import *
from functions import *
from world import World
from pygame.locals import *


def simulate():
    WINDOW_SIZE = (600, 600)

    NUMBER_OF_PARTICLES = -1  # if 0 nothing is generated if < 0  a grid of particles is generated
    PARTICLE_SPEED = 6.7
    PARTICLE_ROTATION = 180
    PARTICLE_SIZE = 5
    RADIUS = 40
    NEIGHBOUR_ATTRACTION = 17  # degrees

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    world = World(WINDOW_SIZE, NUMBER_OF_PARTICLES, PARTICLE_SPEED, PARTICLE_ROTATION,
                  RADIUS, PARTICLE_SIZE, NEIGHBOUR_ATTRACTION)

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Primordial Particle System")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    print(event.key)
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        world.__init__(WINDOW_SIZE, NUMBER_OF_PARTICLES, PARTICLE_SPEED, PARTICLE_ROTATION,
                                       RADIUS, PARTICLE_SIZE, NEIGHBOUR_ATTRACTION)

                if event.type == MOUSEBUTTONDOWN:
                    world.addParticle(mouse_pos, event.button)

        if not pause:
            world.update()

        if frameCount % 1 == 0:
            renderWorld(window, world)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()
