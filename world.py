from particle import Particle
from random import randrange
from functions import *
from vector import Vec4


class World:

    def __init__(self, worldSize, numberParticles, speed, rotation, radius, particleSize, attraction):
        self.worldBounds = Vec4(0, 0, worldSize[0], worldSize[1])
        self.speed = speed
        self.rotation = rotation  # degrees
        self.attraction = attraction
        self.radius = radius
        self.particleSize = particleSize
        self.particles = []

        self.generateNewParticles(numberParticles)

    def generateNewParticles(self, numberParticles):
        self.particles = []
        if numberParticles < 0:
            for y in range(0, self.worldBounds.h, 55):
                for x in range(0, self.worldBounds.w, 55):
                    if y % 2 == 0 and x % 2 != 0 or y % 2 != 0 and x % 2 == 0:
                        o = randrange(0, 360)

                        newParticle = Particle(x, y, o)

                        self.particles.append(newParticle)
        else:
            for i in range(numberParticles):
                x = randrange(self.worldBounds.x, self.worldBounds.x + self.worldBounds.w)
                y = randrange(self.worldBounds.y, self.worldBounds.y + self.worldBounds.h)
                o = randrange(0, 360)

                newParticle = Particle(x, y, o)

                self.particles.append(newParticle)

    def update(self):
        self.countNeighbours()
        self.moveParticles()

    def moveParticles(self):
        for particle in self.particles:
            particle.move(self.rotation, self.speed, self.attraction)

    def addParticle(self, mousePos, mouseAction):
        x = mousePos[0]
        y = mousePos[1]
        o = randrange(0, 360)
        self.particles.append(Particle(x, y, o))

    def countNeighbours(self):
        for particle in self.particles:

            particle.clearNeighbours()
            vecA = particle.getPos()

            for oParticle in self.particles:
                if oParticle.getPos() != particle.getPos():
                    vecB = oParticle.getPos()

                    if distVec2(vecA, vecB) <= self.radius:
                        particle.addNeighbours(oParticle)

    def getParticles(self):
        return self.particles
