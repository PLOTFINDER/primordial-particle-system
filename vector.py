class Vec2:

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def getX(self):
        return self.x

    def setX(self, x):
        self.x = x

    def getY(self):
        return self.x

    def setY(self, y):
        self.y = y

    def __str__(self):
        return " ,".join([str(self.x), str(self.y)])


class Vec3:

    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def getX(self):
        return self.x

    def setX(self, x):
        self.x = x

    def getY(self):
        return self.x

    def setY(self, y):
        self.y = y

    def getZ(self):
        return self.z

    def setZ(self, z):
        self.z = z

    def __str__(self):
        return " ,".join([str(self.x), str(self.y), str(self.z)])


class Vec4:

    def __init__(self, x=0, y=0, w=0, h=0):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def getX(self):
        return self.x

    def setX(self, x):
        self.x = x

    def getY(self):
        return self.x

    def setY(self, y):
        self.y = y

    def getW(self):
        return self.w

    def setW(self, w):
        self.w = w

    def getH(self):
        return self.h

    def setH(self, h):
        self.h = h

    def __str__(self):
        return " ,".join([str(self.x), str(self.y), str(self.w), str(self.h)])
