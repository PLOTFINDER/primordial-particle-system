import math
from vector import Vec2


def distVec2(vectorA, vectorB):
    return math.sqrt((vectorA.x - vectorB.x) ** 2 + (vectorA.y - vectorB.y) ** 2)


def degreeToVector2(degree):
    radians = degree * (math.pi / 180)
    vector = Vec2(math.cos(radians), math.sin(radians))
    return vector


def angleFrom3Vectors(vectorA, vectorB, vectorC):
    result = math.atan2(vectorC.y - vectorA.y, vectorC.x - vectorA.x) - math.atan2(vectorB.y - vectorA.y,
                                                                                   vectorB.x - vectorA.x)
    return math.degrees(result)
